USE movie_db;
LOAD DATA INFILE 'varlibmysql-filesmovies.csv' 
INTO TABLE movies
FIELDS TERMINATED BY ','
ENCLOSED BY '';

LOAD DATA INFILE 'varlibmysql-filescountries.csv' 
INTO TABLE countries
FIELDS TERMINATED BY ',';

LOAD DATA INFILE 'varlibmysql-filesstars.csv' 
INTO TABLE stars
FIELDS TERMINATED BY ',';

LOAD DATA INFILE 'varlibmysql-filesmovie_stars.csv' 
INTO TABLE movie_stars
FIELDS TERMINATED BY ',';

LOAD DATA INFILE 'varlibmysql-filesdirectors.csv' 
INTO TABLE directors
FIELDS TERMINATED BY ',';

LOAD DATA INFILE 'varlibmysql-filesmovie_directors.csv' 
INTO TABLE movie_directors
FIELDS TERMINATED BY ',';

LOAD DATA INFILE 'varlibmysql-filesproducer_countries.csv' 
INTO TABLE producer_countries
FIELDS TERMINATED BY ',';

LOAD DATA INFILE 'varlibmysql-filesproducer_countries.csv' 
INTO TABLE producer_countries
FIELDS TERMINATED BY ',';

LOAD DATA INFILE 'varlibmysql-filesgenres.csv' 
INTO TABLE genres
FIELDS TERMINATED BY ',';

LOAD DATA INFILE 'varlibmysql-fileslanguages.csv' 
INTO TABLE languages
FIELDS TERMINATED BY ',';

show variables like secure_file_priv
